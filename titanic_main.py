'''
@Author: CY Peng
@Date: 2019-07-25
@LastEditTime: 2019-07-25

DataVer  - Author - Note
2019/07/25    CY    First Release(20190725-R 1.0.0)
2019/11/27    CY    ML Lib New Function Included
2019/12/2     CY    add the CMD mode, IDE mode
2019/12/4     CY    add the model_organization
2019/12/23    CY    add the advanced features engineering and training stage
'''
import sys
import os
import re
import ast
from configparser import ConfigParser
import titanic_preprocessing as titanic_db
import titanic_model_creator as titanic_model
import bclfMultiModelTool as MMT

def set_titanic_database(database_folder = "database", json_file_path = "."):
    titanic_db.SetKaggleDataset(database_folder, json_file_path)

def exploratory_data_analysis_stage(file = "train",
                                    database_folder = "database"):
    titanic_db.EDABasicProfile(file, database_folder)
    titanic_db.EDABasicStatistics(file, database_folder)
    try:
        titanic_db.EDABasicStatisticsLabeledCorr(filename, database_folder)
    except:
        pass

def features_selection_engineering(train_file = "train",
                                   test_file = "test",
                                   model_path = os.path.join(".","EDA_output","train"),
                                   database_folder = "database"):
    titanic_db.FeaturesEngineering(train_file, database_folder)
    titanic_db.FeaturesEngineeringFit(test_file, database_folder, model_path)

def adv_features_selection_engineering(train_file = "train",
                                       test_file = "test",
                                       model_path = os.path.join(".","EDA_output","train"),
                                       database_folder = "database"):
    titanic_db.AdvFeaturesEngineering(train_file, database_folder)
    titanic_db.AdvFeaturesEngineeringFit(test_file, database_folder, model_path)

def learning_analysis(file = "train", database_folder = "database"):
    titanic_db.EDAComponentsAnalysis(file, database_folder)
    titanic_db.EDADTDumpROC(file, database_folder)

def training_stage(raw_data_file = 'train',
                   database_folder = "database",
                   train_file_tail_tag = None,
                   features_engineering_model_path = os.path.join(".","EDA_output","train"),
                   model_config_filename='models_config_test'):
    titanic_model.nested_training_stage(raw_data_file,
                                        database_folder,
                                        train_file_tail_tag = train_file_tail_tag,
                                        features_engineering_model_path = features_engineering_model_path,
                                        model_config_filename =model_config_filename)

def adv_training_stage(raw_data_file = 'train',
                       database_folder = "database",
                       train_file_tail_tag = None,
                       features_engineering_model_path = os.path.join(".","EDA_output","train"),
                       model_config_filename='models_config_test'):
    titanic_model.adv_nested_training_stage(raw_data_file,
                                            database_folder,
                                            train_file_tail_tag = train_file_tail_tag,
                                            features_engineering_model_path = features_engineering_model_path,
                                             model_config_filename =model_config_filename)

def train_adv_exploratory_data_analysis_stage(file = "train",
                                        database_folder = "database"):
    titanic_db.EDAAdvStatistics(file, database_folder)

def val_adv_exploratory_data_analysis_from_models_stage(model_path = ".\ML_output"):
    titanic_db.EDAStatisticsFromModels(model_path)
										
def inv_features_engineering(model_path  = ".\ML_output", inv_type = 'std', inv_model_path = os.path.join(".","EDA_output","train"),):
    titanic_db.MultiModelsDataInverse(model_path , inv_type, inv_model_path = inv_model_path)

def prediction_stage(pred_file = "test",
                     database_folder = "database",
                     model_folder = './ML_output',
                     model_name = 'best_clf'):
    titanic_model.prediction_stage(pred_file, database_folder, model_folder, model_name)

def voting_prediction_stage(database_folder = "database", 
                            train_filename = 'train', 
                            test_filename = 'test',
                            model_folder_list = '', 
                            modelname_list = '', 
                            column_selector_list = '',
							**kwargs):

    titanic_model.multi_models_prediction_stage(database_folder, 
	                                            train_filename, 
	                                            test_filename,
                                                model_folder_list,
                                                modelname_list,
								                column_selector_list,
                                                **kwargs)

def model_organization(model_path = ".\ML_output", **kwargs):
    MMT.model_organization(model_path, **kwargs)

def main():
    mode = check_env()
    if (1 == mode):
        cmd_mode()
    if (2 == mode):
        ide_mode()
    #webcam_application()

def voting_prediction_stage_para_setting(config):
    kewgs = {}
    for para in config[config['Execution']['scope']]:
        if ('database_folder'== para) or ('train_filename'== para) or ('test_filename'== para) or ('refit' == para):
            kewgs[para] = config[config['Execution']['scope']][para]
        elif "column_selector_list" == para:
            kewgs[para] = []
            string_array = config[config['Execution']['scope']][para].split(';')
            for idx, string_list in enumerate(string_array):
                kewgs[para].append(ast.literal_eval(string_list))
        else:
            kewgs[para] = ast.literal_eval(config[config['Execution']['scope']][para])
    print('Execution Scope {}'.format(config['Execution']['scope']))
    print('Parameters Setting {}'.format(kewgs))
    return kewgs

def cmd_mode():
    # from ConfigParser import ConfigParser # for python3
    data_file = sys.argv[1]
    config = ConfigParser()
    config.read(data_file)
    kewgs = {}
    for para in config[config['Execution']['scope']]:
        pattern = re.compile(r'^[-+]?[-0-9]\d*\.\d*|[-+]?\.?[0-9]\d*$')
        flag = pattern.match(config[config['Execution']['scope']][para])
        if flag:
            try:
                kewgs[para] = float(config[config['Execution']['scope']][para])
            except:
                pass
            try:
                kewgs[para] = int(config[config['Execution']['scope']][para])
            except:
                pass
        else:
            kewgs[para] = config[config['Execution']['scope']][para]
    """
    for item in config.sections():
        kewgs[item] = {}
        for para in config[item]:
            kewgs[item][para] = config[item][para]
    """
    if 'voting_prediction_stage'  != config['Execution']['scope']:
       print('Execution Scope {}'.format(config['Execution']['scope']))
       print('Parameters Setting {}'.format(kewgs))

    if 'set_titanic_database'== config['Execution']['scope']:
        set_titanic_database(**kewgs)
    if 'train_exploratory_data_analysis_stage'== config['Execution']['scope']:
        exploratory_data_analysis_stage(**kewgs)
    if 'test_exploratory_data_analysis_stage'== config['Execution']['scope']:
        exploratory_data_analysis_stage(**kewgs)
    if 'features_selection_engineering'== config['Execution']['scope']:
        features_selection_engineering(**kewgs)
    if 'train_nor_ord_ohe_bi_learning_analysis'== config['Execution']['scope']:
        learning_analysis(**kewgs)
    if 'train_std_ord_ohe_bi_learning_analysis' == config['Execution']['scope']:
        learning_analysis(**kewgs)
    if 'train_nor_ord_ohe_bi_training_stage' == config['Execution']['scope']:
        training_stage(**kewgs)
    if 'train_std_ord_ohe_bi_training_stage' == config['Execution']['scope']:
        training_stage(**kewgs)
    if 'prediction_stage' == config['Execution']['scope']:
        prediction_stage(**kewgs)
    if 'model_organization' == config['Execution']['scope']:
        model_organization(**kewgs)
    if 'train_adv_exploratory_data_analysis_stage' == config['Execution']['scope']:
        train_adv_exploratory_data_analysis_stage(**kewgs)
    if 'val_adv_exploratory_data_analysis_from_models_stage' == config['Execution']['scope']:
        val_adv_exploratory_data_analysis_from_models_stage(**kewgs)
    if 'inv_features_engineering' == config['Execution']['scope']:
        inv_features_engineering(**kewgs)
    if 'adv_features_selection_engineering' == config['Execution']['scope']:
	    adv_features_selection_engineering(**kewgs)
    if 'adv_' and '_training_stage' in config['Execution']['scope']: 
        adv_training_stage(**kewgs)
    if 'voting_prediction_stage'  == config['Execution']['scope']:
        kewgs = voting_prediction_stage_para_setting(config)
        voting_prediction_stage(**kewgs)
    print('Completed!')

def ide_mode():
    set_titanic_database(database_folder="database", json_file_path = ".")
    exploratory_data_analysis_stage(file="train",
                                    database_folder="database")
    exploratory_data_analysis_stage(file="test",
                                    database_folder="database")
    features_selection_engineering(train_file="train",
                                   test_file="test",
                                   model_path=os.path.join(".", "EDA_output", "train"),
                                   database_folder="database")
    learning_analysis(file="train_nor_ohe_bi", database_folder="database")
    learning_analysis(file="train_std_ohe_bi", database_folder="database")          
    training_stage(raw_data_file = 'train',
                   database_folder = "database",
                   train_file_tail_tag = "_nor_ohe_bi",
                   features_engineering_model_path = os.path.join(".","EDA_output","train"),
                   model_config_filename='models_config_test')
    training_stage(raw_data_file = 'train',
                   database_folder = "database",
                   train_file_tail_tag = "_std_ohe_bi",
                   features_engineering_model_path = os.path.join(".","EDA_output","train"),
                   model_config_filename='models_config_test')
    model_organization(model_path = ".\ML_output")
    train_adv_exploratory_data_analysis_stage(file = "train",
                                        database_folder = "database")
    val_adv_exploratory_data_analysis_from_models_stage(model_path = ".\ML_output")
    adv_features_selection_engineering(train_file="train",
                                   test_file="test",
                                   model_path=os.path.join(".", "EDA_output", "train"),
                                   database_folder="database")
    inv_features_engineering(model_path  = ".\ML_output", inv_type = 'std', inv_model_folder = ".\EDA_output", inv_model_name = 'train')
    adv_training_stage(raw_data_file = 'train',
                       database_folder = "database",
                       train_file_tail_tag = None,
                       features_engineering_model_path = os.path.join(".","EDA_output","train"),
                       model_config_filename='models_config_test')

def check_env():
    print('Check environment variables: {}'.format(sys.argv))
    if (len(sys.argv) > 1):
        print("CMD Mode, Author: CY")
        return 1
    else:
        print("IDE Mode, Author: CY")
        return 2

if __name__ == "__main__":
    main()