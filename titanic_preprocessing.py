'''
@Author: CY Peng
@Date: 2019-07-25
@LastEditTime: 2019-11-27

DataVer  - Author - Note
2019/07/25    CY    First Release(20190725-R 1.0.0)
2019/11/27    CY    ML Lib New Function Included
2019/12/13    CY    Add multi type/target plot
2019/12/23    CY    add the advanced features engineering and training stage
2019/12/29    CY    Feature Engineering Function Check
2019/12/31    CY    Feature Engineering Function Bug Fixed and Update
'''
import kaggle_login as kaggle
import os
import glob
import pandas as pd
import CommonUtility as CU
import H5FileManager as H5FM
import numpy as np
import h5py
import pickle
import matplotlib.pyplot as plt
from pandas.plotting import table
import seaborn as sns
import re
from matplotlib.colors import ListedColormap

# Constom ML Lib
import ExploratoryDataAnalysis as EDA
import LearningAnalysis as LA
import PreprocessingUtility as PU
import SamplingLib as SM
import ModelEvaluation as ME
import MultiModelTool as MMT

outputFolder = "EDA_output"
CU.createOutputFolder(outputFolder)

"""
Basic Preprocessing Utility
"""
def read_data(filename):
    data = pd.read_csv(filename+".csv")
    #data = data.fillna(value="unknown")
    return data

def SetKaggleDataset(database_folder, json_file_path):
    #database_folder = os.path.join(".","database")
    file_list = glob.glob(os.path.join(database_folder, "*.csv"))
    if len(file_list) < 3:
        L = kaggle.kaggle_login(json_file_path = json_file_path)
        os.system('kaggle competitions download titanic -p ' + database_folder)
        L.remove_file()
    print("Database Download Success!")

def EDABasicProfile(filename, database_folder):
    data = read_data(os.path.join(database_folder, filename))
    EDA.df_data_check(data, outputFolder, filename)
    EDA.df_data_type_check(data, outputFolder, filename)

"""
Basic Data Analysis Utility
"""
def df_data_numerical_data_distribution_plot_check(df_data, check_tag, raw_num_data_type, num2cat_data_type, filename, fig_size):
    EDA.df_data_multi_type_plot(df_data.dropna(), raw_num_data_type, None, outputFolder, filename+check_tag, 
                                plot_type = 'Distribution', ylabel_list = ["No. of Passenger(s)"], fig_size = fig_size)
    EDA.df_data_multi_type_plot(df_data.fillna(value="unknown"), num2cat_data_type, None, outputFolder, filename+check_tag, plot_type = 'Count', ylabel_list = ["No. of Passenger(s)"], fig_size = fig_size)

def df_data_categorical_data_distribution_plot_check(df_data, check_tag, raw_cat_data_type, filename, fig_size):
    EDA.df_data_multi_type_plot(df_data.fillna(value="unknown"), raw_cat_data_type, None, outputFolder, filename+check_tag, 
                                plot_type = 'Count', ylabel_list = ["No. of Passenger(s)"], fig_size = fig_size)

def EDAComponentsAnalysis(filename, database_folder):
    data = read_data(os.path.join(database_folder, filename))
    data = data.drop(columns=['PassengerId', 'Survived'],axis = 1)
    pcaObj = LA.PrincipleComponentAnalysis(data, outputFolder, filename)
    LA.VisualizingN2MEigenVector(pcaObj, data, (0, 5), outputFolder, filename)

def EDADTDumpROC(filename, database_folder):
    data = read_data(os.path.join(database_folder, filename))
    y = data.loc[:, 'Survived'].to_frame()
    X = data.drop(columns=['PassengerId', 'Survived'], axis=1)
    ME.df_data_features_ranking_roc_curve(X, y, outputFolder, filename) 

"""
Basic Statistics & EDA Function

Note
Name: Object Data Type -> Categorical Data Type
Ticket: Object Data Type -> Numerical Data Type & Categorical Data Type
Cabin: Object Data Type -> Categorical Data Type
"""
def num_basic_processing(df_data):
    # Age
    df_data['AgeMap'] = df_data['Age']
    df_data.loc[df_data['Age'].dropna().index, 'AgeMap'] = 'Numbers'
    df_data.loc[df_data['Age'].isnull().index, 'AgeMap'] = 'unknown'

    return df_data

def cat_basic_processing(df_data):
    # Name
    df_data['NameMap'] = df_data.loc[:, 'Name'].apply(CU.get_title)
    df_data['NameMap'] = df_data.loc[:, 'NameMap'].replace(
        ['Lady', 'Countess', 'Capt', 'Col', 'Don', 'Dr', 'Major', 'Rev', 'Sir', 'Jonkheer', 'Dona'], 'Rare')
    df_data['NameMap'] = df_data.loc[:, 'NameMap'].replace('Mlle', 'Miss')
    df_data['NameMap'] = df_data.loc[:, 'NameMap'].replace('Ms', 'Miss')
    df_data['NameMap'] = df_data.loc[:, 'NameMap'].replace('Mme', 'Mrs')
    #title_mapping = {"Mr": 1, "Miss": 2, "Mrs": 3, "Master": 4, "Rare": 5}
    #df_data['NameTitle'] = df_data['Name'].map(title_mapping)
    #df_data['NameTitle'] = df_data['Name'].fillna(0)
    
    # Ticket
    df_data['NumTicket'] = df_data.loc[:, 'Ticket']
    df_data['CatTicket'] = df_data.loc[:, 'Ticket'].apply(CU.get_title)
    df_data['CatTicket'] = df_data.loc[:, 'Ticket'].astype(str).str.contains('[A-Za-z]', regex=True).map({False: 0, True:1})
    df_data.loc[df_data.loc[:, 'CatTicket'] == 1, 'NumTicket'] = -1
    df_data['NumTicket'] = df_data.loc[df_data.loc[:, 'CatTicket'] == 0, 'NumTicket'].astype(int)
    df_data['NumTicketMap'] = df_data['NumTicket']
    df_data.loc[df_data['NumTicket'] == -1, 'NumTicket'] = np.nan
    #df_data['NumTicketMap'] = df_data.loc[:, 'NumTicketMap'].astype(int)
    
    # Cabin
    df_data['CabinMap'] = df_data.loc[:, 'Cabin'].astype(str).str[0]
    df_data['CabinMap'] = df_data.loc[:, 'CabinMap'].replace('n', 'unknown')
    
    # Embarked
    df_data['EmbarkedMap'] = df_data.loc[:,'Embarked'].map({'C':0, 'S': 1, 'Q': 2, np.nan: 3}).astype(int)
    
    # Pclass
    df_data['PclassMap'] = df_data.loc[:,'Pclass'].map({1:0, 2: 1, 3: 2}).astype(int)
    
    # Sex
    df_data['SexMap'] = df_data.loc[:, 'Sex'].map({'female': 0, 'male': 1}).astype(int)
    
    return df_data

def eda_basic_statistics_data_type_def():
    # numerical data type
    raw_num_data_type = ['PassengerId', 'Age', 'SibSp', 'Parch', 'Fare', 'NumTicketMap'] #['PassengerId', 'Age', 'SibSp', 'Parch', 'NumTicket']
    num2cat_data_type = ['AgeMap', 'CatTicket']
    num_data_type_ylabel = ['ID Number', 'Years Old', 'No.', 'No.', 'Ticket No., N Type', 'Dollars']
    	
    # categorical data type
    raw_cat_data_type = ['Sex', 'Name', 'Pclass', 'Cabin', 'Embarked']
    map_cat_data_type = ['SexMap', 'NameMap', 'PclassMap', 'CabinMap', 'EmbarkedMap']
    cat_data_type_yLabel_list = ['']
	
	# label data type
    label_data_type = ['Survived']
    drop_out_data_type = ['Cabin'] # Categorical Data
    return raw_num_data_type, num2cat_data_type, num_data_type_ylabel, raw_cat_data_type, map_cat_data_type, cat_data_type_yLabel_list, label_data_type, drop_out_data_type

def EDABasicStatistics(filename, database_folder):
    df_data = read_data(os.path.join(database_folder, filename))

    # Preprocessing
    df_data = num_basic_processing(df_data)
    df_data = cat_basic_processing(df_data)
    raw_num_data_type, num2cat_data_type, num_data_type_ylabel, raw_cat_data_type, map_cat_data_type, cat_data_type_yLabel_list, label_data_type, drop_out_data_type = eda_basic_statistics_data_type_def()
    # Data Check
    df_data_numerical_data_distribution_plot_check(df_data.dropna(), '_basic', raw_num_data_type, num2cat_data_type, filename, (10, 10))
    df_data_categorical_data_distribution_plot_check(df_data, '_basic', raw_cat_data_type, filename, (10, 10))

    # Numerical Data Type
    EDA.df_data_ANOVA_test(df_data.dropna(), raw_num_data_type, outputFolder, filename)
    EDA.df_data_numerical_type_describe(df_data, outputFolder, filename) #.fillna(value=-1)
    #EDA.df_data_multi_type_plot(df_data.fillna(value=-1), numerical_data_type, None, outputFolder, filename, 
    #                            plot_type = 'Distribution', ylabel_list = ["No. of Passenger(s)"], fig_size = (10, 10))
    EDA.df_data_numerical_type_correlation_heatmap(df_data.dropna(), raw_num_data_type, outputFolder, filename)
    
    #EDA.df_data_multi_type_plot(df_data.fillna(value="unknown"), bi_categorical_data_type, None, outputFolder, filename+'_single', plot_type = 'Count', ylabel_list = ["No. of Passenger(s)"], fig_size = (10, 10))
    
    # All Data Type
    EDA.df_data_percentage_description(df_data.fillna(value="unknown"), None, outputFolder, filename)

def EDABasicStatisticsLabeledCorr(filename, database_folder):
    df_data = read_data(os.path.join(database_folder, filename))

    # Preprocessing
    df_data = num_basic_processing(df_data)
    df_data = cat_basic_processing(df_data)
    raw_num_data_type, num2cat_data_type, num_data_type_ylabel, raw_cat_data_type, map_cat_data_type, cat_data_type_yLabel_list, label_data_type, drop_out_data_type = eda_basic_statistics_data_type_def()
	
	# Numerical Data Type
    EDA.df_data_multi_type_plot(df_data.dropna(), raw_num_data_type, label_data_type[0], outputFolder, filename, 
                                plot_type = 'Box', ylabel_list = numerical_data_type_ylabel, fig_size = (10, 10))
    EDA.df_data_numerical_type_pairscatter_plot(df_data.fillna(value=-1), raw_num_data_type, label_data_type[0],
                                                outputFolder, filename)

    # Categorical Data Type
    EDA.df_data_percentage_description(df_data.fillna(value="unknown"), label_data_type[0], outputFolder, filename)
    EDA.df_data_multi_type_plot(df_data.fillna(value="unknown"), raw_cat_data_type, label_data_type[0], outputFolder, filename,plot_type = 'Count', ylabel_list = ["No. of Passenger(s)"], fig_size = (10, 10))     
    EDA.df_data_multi_type_percentage_value_bar_plot(df_data.fillna(value="unknown"), raw_cat_data_type,
                                                     label_data_type[0], outputFolder, filename, ylabel_list = None)

"""
Features Engineering based on the Basic EDA
Name: Object Data Type -> Categorical Data Type
Ticket: Object Data Type -> Numerical Data Type
Cabin: Object Data Type -> Categorical Data Type
Sex: Binary Data Type -> 0/1 Data Type
"""

def features_transformation(df_data, filename, model_file_path):
    #model_file_path = os.path.join(outputFolder, filename)
    # Data Type
    numerical_data_type = ['Age', 'SibSp', 'Parch', 'NumTicketMap', 'Fare']
    nom_cat_data_type = ['NameMap', 'Embarked']
    ord_cat_data_type = ['PclassMap']
    
    # Order Data 
    ord_cat_data = df_data.loc[:, ord_cat_data_type]

    # Nominal Data
    try:
        # Nominal Categorical Data Type: One Hot Encoder + Imputation
        with open(model_file_path + "_ohe.sav", 'rb') as file:
             oheObj = pickle.load(file)
        print('Ohe Transfromation Model Load Completed!')	
    except:
        # Nominal Categorical Data Type: One Hot Encoder + Imputation
        #print(pd.isnull(df_data.loc[:, categorical_data_type]).any(1).nonzero()[0])
        #print(list(np.where(df_data[categorical_data_type].isna()[0])))
        oheObj = PU.cat_one_hot_encoder(df_data.loc[:, nom_cat_data_type].fillna(value='unknown'), outputFolder, filename, sparse=False)
        print('Ohe Transfromation Completed!')
    
    # Nominal Categorical Data Type: One Hot Encoder + Imputation
    ohe_cat_data = oheObj.fit_transform(df_data.loc[:, nom_cat_data_type].fillna(value='unknown'))
    ohe_cat_data = pd.DataFrame(ohe_cat_data, columns=oheObj.get_feature_names())
    
    ohe_cat_data = PU.convert_missing_ohe_to_nan(ohe_cat_data, oheObj.get_feature_names(), missing_name='unknown')
    try:
        imp_ohe_cat_data = PU.num_fastKNN(ohe_cat_data, 3000, outputFolder, filename + '_ohe', k=30, **{})
        imp_ohe_cat_data.columns = ohe_cat_data.columns
    except:
        imp_ohe_cat_data = ohe_cat_data
    
    # Numerical Data Type: Standardization, Normalization + Imputation
    try:
        imp_num_data = PU.num_fastKNN(df_data.loc[:, numerical_data_type].fillna(value=np.nan), 3000, outputFolder,
                                      filename + '_num', k=30, **{})
        imp_num_data.columns = df_data.loc[:, numerical_data_type].columns
    except:
        imp_num_data = df_data.loc[:, numerical_data_type]
    
    try:
        with open(model_file_path + "_std.sav", 'rb') as file:
             stdDataObj = pickle.load(file)
        print('Std Transfromation Model Load Completed!')
        
        with open(model_file_path + "_nor.sav", 'rb') as file:
             norDataObj = pickle.load(file)
        print('Nor Transfromation Model Load Completed!')
    except: 
        stdDataObj = PU.num_standardization(imp_num_data.loc[:, numerical_data_type], outputFolder, filename, **{})
        print('Std Transfromation Completed!')
    
        norDataObj = PU.num_normalizer(imp_num_data.loc[:, numerical_data_type], outputFolder, filename, **{})
        print('Nor Transfromation Completed!')
        
    std_imp_num_data = stdDataObj.transform(imp_num_data.loc[:, numerical_data_type])
    std_imp_num_data = pd.DataFrame(std_imp_num_data, columns=numerical_data_type)
    
    nor_imp_num_data = norDataObj.transform(imp_num_data.loc[:, numerical_data_type])
    nor_imp_num_data = pd.DataFrame(nor_imp_num_data, columns=numerical_data_type)
    
    return imp_num_data, std_imp_num_data, nor_imp_num_data, ord_cat_data, imp_ohe_cat_data

def FeaturesEngineering(filename, database_folder):
    df_data = read_data(os.path.join(database_folder, filename))

    # Preprocessing
    df_data = num_basic_processing(df_data)
    df_data = cat_basic_processing(df_data)
    bi_cat_data_type = ['SexMap']
    label_data_type = ['Survived']

    # Features Transfromation
    imp_num_data, std_imp_num_data, nor_imp_num_data, ord_cat_data, imp_ohe_cat_data = features_transformation(df_data, filename, None)

    # Data Concat
    num_ohe = pd.concat(
        [df_data.loc[:,'PassengerId'], imp_num_data, ord_cat_data, imp_ohe_cat_data, df_data.loc[:, bi_cat_data_type], df_data.loc[:, label_data_type]], axis=1)
    std_ohe = pd.concat(
        [df_data.loc[:,'PassengerId'], std_imp_num_data, ord_cat_data, imp_ohe_cat_data, df_data.loc[:, bi_cat_data_type], df_data.loc[:, label_data_type]], axis=1)
    nor_ohe = pd.concat(
        [df_data.loc[:,'PassengerId'], nor_imp_num_data, ord_cat_data, imp_ohe_cat_data, df_data.loc[:, bi_cat_data_type], df_data.loc[:, label_data_type]], axis=1)

    # Data Storing
    num_ohe.to_csv(os.path.join(database_folder, filename + "_num_ord_ohe_bi.csv"), index=False)
    std_ohe.to_csv(os.path.join(database_folder, filename + "_std_ord_ohe_bi.csv"), index=False)
    nor_ohe.to_csv(os.path.join(database_folder, filename + "_nor_ord_ohe_bi.csv"), index=False)

def FeaturesEngineeringFit(filename, database_folder, model_file_path):
    df_data = read_data(os.path.join(database_folder, filename))

    # Preprocessing
    df_data = num_basic_processing(df_data)
    df_data = cat_basic_processing(df_data)
    bi_cat_data_type = ['SexMap']

    # Features Transfromation
    imp_num_data, std_imp_num_data, nor_imp_num_data, ord_cat_data, imp_ohe_cat_data = features_transformation(df_data, filename, model_file_path)

    # Data Concat
    num_ohe = pd.concat(
        [df_data.loc[:,'PassengerId'], imp_num_data, ord_cat_data, imp_ohe_cat_data, df_data.loc[:, bi_cat_data_type]], axis=1)
    std_ohe = pd.concat(
        [df_data.loc[:,'PassengerId'], std_imp_num_data, ord_cat_data, imp_ohe_cat_data, df_data.loc[:, bi_cat_data_type]], axis=1)
    nor_ohe = pd.concat(
        [df_data.loc[:,'PassengerId'], nor_imp_num_data, ord_cat_data, imp_ohe_cat_data, df_data.loc[:, bi_cat_data_type]], axis=1)

    # Data Storing
    num_ohe.to_csv(os.path.join(database_folder, filename + "_num_ord_ohe_bi.csv"), index=False)
    std_ohe.to_csv(os.path.join(database_folder, filename + "_std_ord_ohe_bi.csv"), index=False)
    nor_ohe.to_csv(os.path.join(database_folder, filename + "_nor_ord_ohe_bi.csv"), index=False)

"""
Advanced Statistics & EDA Function

Note
Name: Object Data Type -> Categorical Data Type
Ticket: Object Data Type -> Numerical Data Type & Categorical Data Type
Cabin: Object Data Type -> Categorical Data Type
"""
def num_adv_processing(df_data, filename):
    #model_file_path = os.path.join(outputFolder, filename)

    # Age
    df_data['AgeMap'] = df_data['Age']
    #print(df_data['AgeMap'])
    #df_data.loc[df_data['AgeMap'].isnull().index, 'AgeMap'] = np.nan
    #print(df_data['AgeMap'])
    oheObj = PU.cat_one_hot_encoder(df_data.loc[:, ['NameMap']], outputFolder, filename + '_temp_age', sparse=False)
    temp_ohe_cat_data = oheObj.fit_transform(df_data.loc[:, ['NameMap']])
    temp_ohe_cat_data = pd.DataFrame(temp_ohe_cat_data, columns=oheObj.get_feature_names())

    age_mean = df_data['AgeMap'].mean()
    age_max = df_data['AgeMap'].max()
    age_min = df_data['AgeMap'].min()
    df_data['AgeTemp'] = df_data['AgeMap'].dropna().map(lambda x: (x - age_mean) / (age_max - age_min))
    #print(df_data['AgeTemp'])
    #print(temp_ohe_cat_data)
    temp_imp_age = pd.concat([df_data.loc[:, ['AgeTemp', 'SibSp', 'Parch', 'EmbarkedMap']], temp_ohe_cat_data], axis = 1)
    #print(temp_imp_age)
    temp_imp_age = PU.num_fastKNN(temp_imp_age, 3000, outputFolder, filename + '_temp_age', k=60, **{})
     #print(temp_imp_age)
    temp_column_list = []
    temp_column_list.extend(['AgeTemp', 'SibSp', 'Parch', 'EmbarkedMap'])
    temp_column_list.extend(oheObj.get_feature_names())
    #print(temp_column_list)
    temp_imp_age.columns = temp_column_list
	
    df_data['AgeMap'] = temp_imp_age['AgeTemp'].map(lambda x: x*(age_max - age_min) + age_mean)
    #print(df_data['AgeMap'])
    #print(temp_imp_age['AgeTemp'].map(lambda x: x*(age_max - age_min) + age_mean))

    #print(df_data['AgeMap'].isnull())
    #print(sum(df_data['AgeMap'].isnull()))
    #df_data['AgeMap'] = temp_imp_age.loc[:, 'AgeMap']
    #print(df_data['AgeMap'].isnull())
    #print(sum(df_data['AgeMap'].isnull()))
    print('check Age', sum(df_data['AgeMap']=='unknown'))
    print('check Age', sum(df_data['AgeMap'].isnull()))
    #print(temp_data)
    #print(len(temp_data.iloc[:, 0]), len(df_data['AgeMap']))	
	#temp_data.columns = ['AgeMap', temp_ohe_cat_data.columns]
    #df_data['AgeMap'] = temp_data.iloc[:, 0].astype(int)

    # Fare
    df_data['FareMap'] = df_data['Fare']
    try:
        fare_mean = df_data['FareMap'].mean()
        fare_max = df_data['FareMap'].max()
        fare_min = df_data['FareMap'].min()
        df_data['FareTemp'] = df_data['FareMap'].dropna().map(lambda x: (x - fare_mean) / (fare_max - fare_min))

        temp_data_type_list = ['FareTemp', 'PclassMap', 'EmbarkedMap']
        temp_data = PU.num_fastKNN(df_data.loc[:, temp_data_type_list], 3000, outputFolder, filename + '_temp_fare', k=30, **{})
        temp_data.columns = df_data.loc[:, temp_data_type_list].columns
        
        df_data['FareMap'] = temp_data['FareTemp'].map(lambda x: x*(fare_max - fare_min) + fare_mean)
    except:
        pass
    print('check Fare', sum(df_data['FareMap']=='unknown'))
    print('check Fare', sum(df_data['FareMap'].isnull()))

    return df_data

def cat_adv_processing(df_data, filename):
    #model_file_path = os.path.join(outputFolder, filename)

    # Name
    df_data['NameTitle'] = df_data.loc[:, 'Name'].apply(CU.get_title)
    df_data['NameMap'] = df_data.loc[:, 'Name'].apply(CU.get_title).rank(method='dense', ascending=False).astype(int)
    #print(df_data['NameMap'])
    print('check NameTitle', sum(df_data['NameTitle']=='unkwnown'))
    print('check NameTitle', sum(df_data['NameTitle'].isnull()))
    print('check NameMap', sum(df_data['NameMap']=='unkwnown'))
    print('check NameMap', sum(df_data['NameMap'].isnull()))

    # Ticket
    df_data['NumTicket'] = df_data.loc[:, 'Ticket']
    df_data['CatTicket'] = df_data.loc[:, 'Ticket'].astype(str).str[0]
    df_data['CatTicketFlag'] = df_data.loc[:, 'Ticket'].astype(str).str.contains('[A-Za-z]', regex=True).map({False: 0, True:1})
    df_data.loc[df_data.loc[:, 'CatTicketFlag'] == 1, 'NumTicket'] = -1
    df_data['NumTicket'] = df_data.loc[df_data.loc[:, 'CatTicketFlag'] == 0, 'NumTicket'].astype(int)
    df_data['NumTicketMap'] = df_data['NumTicket']
    df_data.loc[df_data['NumTicket'] == -1, 'NumTicket'] = np.nan
    #df_data['NumTicketMap'] = df_data.loc[:, 'NumTicketMap'].astype(int)
	
    df_data['CatTicketMap'] = df_data.loc[:, 'CatTicket'].apply(lambda x: ord(x)-64) #
    df_data.loc[df_data.loc[:, 'CatTicketFlag'] == 0, 'CatTicketMap'] = 0
    #print(df_data['CatTicketMap'])
    
    df_data.loc[df_data.loc[:, 'CatTicketFlag'] == 1, 'NumTicket'] = -1
    df_data['NumTicketMap'] = df_data['NumTicket']
    df_data['NumTicket'] = df_data.loc[df_data.loc[:, 'CatTicketFlag'] == 0, 'NumTicket'].astype(int)
    df_data.loc[df_data['NumTicket'] == -1, 'NumTicket'] = np.nan
    print('check NumTicket', sum(df_data['NumTicket']=='unkwnown'))
    print('check NumTicket', sum(df_data['NumTicket'].isnull()))
    print('check CatTicket', sum(df_data['CatTicket']=='unkwnown'))
    print('check CatTicket', sum(df_data['CatTicket'].isnull()))
    print('check NumTicketMap', sum(df_data['NumTicketMap']=='unkwnown'))
    print('check NumTicketMap', sum(df_data['NumTicketMap'].isnull()))
    print('check CatTicketMap', sum(df_data['CatTicketMap']=='unkwnown'))
    print('check CatTicketMap', sum(df_data['CatTicketMap'].isnull()))

    df_data['Ticket2Family'] = 0
    ticket_count = df_data.groupby('Ticket')['PassengerId'].count()
    family_index = np.where(ticket_count > 1)
    family_ticket_list = ticket_count.index[family_index]
    family_index_list = [index for index in range(len(df_data['Ticket'])) if df_data.loc[index, 'Ticket'] in family_ticket_list]
    df_data.loc[family_index_list,'Ticket2Family'] = 1
    print('check Ticket2Family', sum(df_data['Ticket2Family']=='unkwnown'))
    print('check Ticket2Family', sum(df_data['Ticket2Family'].isnull()))

    # Pclass
    df_data['PclassMap'] = df_data.loc[:,'Pclass'].map({1:0, 2: 1, 3: 2}).astype(int)
    print('check PclassMap', sum(df_data['PclassMap']=='unkwnown'))
    print('check PclassMap', sum(df_data['PclassMap'].isnull()))

    # Embarked
    df_data['EmbarkedMap'] = df_data.loc[:,'Embarked'].map({'C':0, 'S': 1, 'Q': 2, np.nan: np.nan})#.astype(int)  
    try:
        temp_data_type_list = ['PclassMap', 'EmbarkedMap']
        temp_data = PU.num_fastKNN(df_data[temp_data_type_list], 3000, outputFolder, filename + '_temp_embarked', k=30, **{})
        temp_data.columns = df_data.loc[:, temp_data_type_list].columns
        df_data['EmbarkedMap'] = temp_data['EmbarkedMap'].astype(int)
    except:
        df_data['EmbarkedMap'] = df_data['EmbarkedMap'].astype(int)
        pass
    
    print('check EmbarkedMap', sum(df_data['EmbarkedMap']=='unkwnown'))
    print('check EmbarkedMap', sum(df_data['EmbarkedMap'].isnull()))

    # Sex
    df_data['SexMap'] = df_data.loc[:, 'Sex'].map({'female': 0, 'male': 1}).astype(int)
    print('check SexMap', sum(df_data['SexMap']=='unkwnown'))
    print('check SexMap', sum(df_data['SexMap'].isnull()))

    # Cabin
    df_data['CatCabin'] = df_data.loc[:, 'Cabin'].astype(str).str[0]
    df_data['CabinMap'] = df_data.loc[:, 'Cabin'].astype(str).str[0]
    df_data['CabinMap'] = df_data.loc[:, 'CabinMap'].map(lambda x: ord(x)-64)
    df_data.loc[df_data['CatCabin'] == 'n', 'CabinMap'] = 0
    print('check CabinMap', sum(df_data['CabinMap']=='unkwnown'))
    print('check CabinMap', sum(df_data['CabinMap'].isnull()))

    return df_data

def eda_adv_statistics_data_type_def():
    # numerical data type
    raw_num_data_type = ['PassengerId', 'Age', 'SibSp', 'Parch', 'NumTicket', 'Fare']
    num2cat_data_type = ['AgeMap', 'CatTicket']
    map_num_data_type = ['AgeMap', 'SibSp', 'Parch', 'NumTicketMap', 'FareMap']
    num_data_type_ylabel = ['ID Number', 'Years Old', 'No.', 'No.', 'Ticket No., N Type', 'Dollars']
    	
    # categorical data type
    raw_cat_data_type = ['Sex', 'Name', 'Pclass', 'Cabin', 'Embarked', 'CatTicket']
    map_cat_data_type = ['SexMap', 'NameTitle', 'NameMap', 'PclassMap', 'CatCabin', 'CabinMap', 'EmbarkedMap', 'CatTicket', 'CatTicketMap', 'Ticket2Family']
    cat_data_type_yLabel_list = ['']
	
	# label data type
    label_data_type = ['Survived']
    return raw_num_data_type, map_num_data_type, num2cat_data_type, num_data_type_ylabel, raw_cat_data_type, map_cat_data_type, cat_data_type_yLabel_list, label_data_type

def EDAAdvStatistics(filename, database_folder):
    df_data = read_data(os.path.join(database_folder, filename))

    # Preprocessing
    df_data = cat_adv_processing(df_data, filename)
    df_data = num_adv_processing(df_data, filename)
    raw_num_data_type, map_num_data_type, num2cat_data_type, num_data_type_ylabel, raw_cat_data_type, map_cat_data_type, cat_data_type_yLabel_list, label_data_type = eda_adv_statistics_data_type_def()
    # Data Check
    df_data_numerical_data_distribution_plot_check(df_data, '_adv', map_num_data_type, num2cat_data_type, filename, (10, 10))
    df_data_categorical_data_distribution_plot_check(df_data, '_adv', raw_cat_data_type, filename, (10, 10))

    # Group A:
    EDA.df_data_multi_target_plot(df_data.dropna(), 'Age', ['Sex', 'NameTitle'], outputFolder, filename + '_group_A', plot_type = 'Box', ylabel_list = ["No of Passengers"], fig_size = (10, 10)) #
    EDA.df_data_multi_target_plot(df_data.fillna(value="unknown"), 'Sex', ['NameTitle'], outputFolder, filename + '_group_A', plot_type = 'Count', ylabel_list = ["No of Passengers"], fig_size = (10, 10))

    # Group B:
    EDA.df_data_multi_type_plot(df_data.fillna(value="unknown"), ['SibSp', 'Parch'], 'Sex', outputFolder, filename + '_group_B', plot_type = 'Count', ylabel_list = ["No of Passengers"], fig_size = (10, 10))
    EDA.df_data_multi_type_plot(df_data.fillna(value="unknown"), ['SibSp', 'Parch'], 'NameTitle', outputFolder, filename + '_group_B', plot_type = 'Count', ylabel_list = ["No of Passengers"], fig_size = (10, 10))
	
    # Group C:
    EDA.df_data_multi_type_plot(df_data.fillna(value="unknown"), ['NumTicketMap', 'FareMap', 'PclassMap'], 'Embarked', outputFolder, filename + '_group_C', plot_type = 'Box', ylabel_list = ["Ticket No.", "Dollars", "Pclass Level"], fig_size = (10, 10))
    EDA.df_data_multi_type_plot(df_data.fillna(value="unknown"), ['SibSp', 'Parch'], 'Pclass', outputFolder, filename + '_group_C', plot_type = 'Count', ylabel_list = ["No of Passengers"], fig_size = (10, 10))
    EDA.df_data_multi_type_plot(df_data.fillna(value="unknown"), ['SibSp', 'Parch'], 'Embarked', outputFolder, filename + '_group_C', plot_type = 'Count', ylabel_list = ["No of Passengers"], fig_size = (10, 10))

"""
Features Engineering based on the Advanced EDA
Name: Object Data Type -> Categorical Data Type
Ticket: Object Data Type -> Numerical Data Type
Cabin: Object Data Type -> Categorical Data Type
Sex: Binary Data Type -> 0/1 Data Type
"""
def adv_features_transformation(df_data, filename, model_file_path):
    #model_file_path = os.path.join(outputFolder, filename)
    # Note
    # Name: Object Data Type -> Categorical Data Type
    # Ticket: Object Data Type -> Numerical Data Type
    # Cabin: Object Data Type -> Categorical Data Type
    # Embarked: 0-2 Finance Level Order Encoder
    # Sex: Binary Data Type -> 0/1 Data Type
    num_data_type = ['AgeMap', 'SibSp', 'Parch', 'NumTicketMap', 'FareMap'] #'PassengerId'  
    #nom_cat_data_type = []
    ord_cat_data_type = ['NameMap', 'PclassMap', 'EmbarkedMap', 'CatTicketMap', 'Ticket2Family']
    #bi_cat_data_type = ['Sex'] # drop out
    label_data_type = ['Survived']
    #drop_out_data_type = ['Cabin']  # Categorical Data
    
    try:
        # Nominal Categorical Data Type: One Hot Encoder + Imputation
        with open(model_file_path + "_adv_ohe.sav", 'rb') as file:
             oheObj = pickle.load(file)
        print('Adv ohe Transfromation Model Load Completed!')
    
        # Numerical Data Type: Standardization, Normalization
        with open(model_file_path + "_adv_std.sav", 'rb') as file:
             stdDataObj = pickle.load(file)
        print('Adv std Transfromation Model Load Completed!')
    
        with open(model_file_path + "_adv_nor.sav", 'rb') as file:
             norDataObj = pickle.load(file)
        print('Adv nor Transfromation Model Load Completed!')
    
    except:
        # Nominal Categorical Data Type: One Hot Encoder + Imputation
        #print(pd.isnull(df_data.loc[:, categorical_data_type]).any(1).nonzero()[0])
        #print(list(np.where(df_data[categorical_data_type].isna()[0])))
        #oheObj = PU.cat_one_hot_encoder(df_data.loc[:, nom_cat_data_type].fillna(value='unknown'), outputFolder, filename+'_adv', sparse=False)
        print('Adv ohe Transfromation Completed!')
        
        stdDataObj = PU.num_standardization(df_data.loc[:, num_data_type], outputFolder, filename+'_adv', **{})
        print('Adv std Transfromation Completed!')
    
        norDataObj = PU.num_normalizer(df_data.loc[:, num_data_type], outputFolder, filename+'_adv', **{})
        print('Adv nor Transfromation Completed!')
    
    # Nominal Categorical Data Type: One Hot Encoder + Imputation
    #ohe_cat_data = oheObj.fit_transform(df_data.loc[:, nom_cat_data_type].fillna(value='unknown'))
    #ohe_cat_data = pd.DataFrame(ohe_cat_data, columns=oheObj.get_feature_names())
    
    # Order Categorical Data Type:
    ord_cat_data = df_data.loc[:, ord_cat_data_type]
    
    # Numerical Data Type: Standardization, Normalization
    std_num_data = stdDataObj.transform(df_data.loc[:, num_data_type])
    std_num_data = pd.DataFrame(std_num_data, columns=num_data_type)
    
    nor_num_data = norDataObj.transform(df_data.loc[:, num_data_type])
    nor_num_data = pd.DataFrame(nor_num_data, columns=num_data_type) 
    
    # All Data Organization
    # PCA for Ticket (NumTicketMap, CatTicketMap), Fare, Embarked, Pclass Data
    nor_group_c_df_data = pd.concat([nor_num_data.loc[:, ['NumTicketMap', 'FareMap']], ord_cat_data.loc[:, ['CatTicketMap']], df_data.loc[:, ['EmbarkedMap', 'PclassMap']]], axis=1)
    std_group_c_df_data = pd.concat([std_num_data.loc[:, ['NumTicketMap', 'FareMap']], ord_cat_data.loc[:, ['CatTicketMap']], df_data.loc[:, ['EmbarkedMap', 'PclassMap']]], axis=1)
    
    try:
        # Numerical Data Type: PCA
        with open(model_file_path + "_adv_nor_group_c_pca.sav", 'rb') as file:
             pcaNorObj = pickle.load(file)
        print('Adv pca nor Transfromation Model Load Completed!')
    
        with open(model_file_path + "_adv_std_group_c_pca.sav", 'rb') as file:
             pcaStdObj = pickle.load(file)
        print('Adv pca std Transfromation Model Load Completed!')
    except:
        pcaNorObj = LA.PrincipleComponentAnalysis(nor_group_c_df_data, outputFolder, filename+'_adv_nor_group_c')
        LA.VisualizingN2MEigenVector(pcaNorObj, nor_group_c_df_data, (0, 3), outputFolder, filename+'_adv_nor_group_c')
        print('Adv nor Transfromation Completed!')

        pcaStdObj = LA.PrincipleComponentAnalysis(nor_group_c_df_data, outputFolder, filename+'_adv_std_group_c')
        LA.VisualizingN2MEigenVector(pcaStdObj, nor_group_c_df_data, (0, 3), outputFolder, filename+'_adv_std_group_c')
        print('Adv nor Transfromation Completed!')
    
    nor_group_c_df_data_1_pca_data = pd.DataFrame(LA.N2MProjection(pcaNorObj, nor_group_c_df_data, [0, 1]), columns=['group3_nor_pca_1'])
    nor_group_c_df_data_2_pca_data = pd.DataFrame(LA.N2MProjection(pcaNorObj, nor_group_c_df_data, [1, 2]), columns=['group3_nor_pca_2'])
    std_group_c_df_data_1_pca_data = pd.DataFrame(LA.N2MProjection(pcaStdObj, std_group_c_df_data, [0, 1]), columns=['group3_std_pca_1'])
    std_group_c_df_data_2_pca_data = pd.DataFrame(LA.N2MProjection(pcaStdObj, std_group_c_df_data, [1, 2]), columns=['group3_std_pca_2'])

    # Data Organization: AgeMap, NameMap, SibSp, Parch, Ticket2Family, NumTicketMap, CatTicketMap, Fare, Embarked, Pclass Data
    num_data_group_list = ['AgeMap', 'NameMap', 'SibSp', 'Parch', 'Ticket2Family', 'NumTicketMap', 'CatTicketMap', 'FareMap', 'EmbarkedMap', 'PclassMap'] #, 'CabinMap'
    num_df_data = df_data.loc[:,num_data_group_list]

    # Data Organization: AgeMap, NameMap
    nor_group_a_df_data = pd.concat([nor_num_data.loc[:, 'AgeMap'], ord_cat_data.loc[:, 'NameMap']],axis = 1)
    std_group_a_df_data = pd.concat([std_num_data.loc[:, 'AgeMap'], ord_cat_data.loc[:, 'NameMap']],axis = 1)
    
    # Data Organization: SibSp, Parch, Ticket2Family
    nor_group_b_df_data = pd.concat([nor_num_data.loc[:, ['SibSp', 'Parch']], ord_cat_data.loc[:, ['Ticket2Family']]], axis = 1)
    std_group_b_df_data = pd.concat([std_num_data.loc[:, ['SibSp', 'Parch']], ord_cat_data.loc[:, ['Ticket2Family']]], axis = 1)   
    
    return num_df_data, nor_group_a_df_data, std_group_a_df_data, nor_group_b_df_data, std_group_b_df_data, nor_group_c_df_data_1_pca_data, nor_group_c_df_data_2_pca_data, std_group_c_df_data_1_pca_data, std_group_c_df_data_2_pca_data

def AdvFeaturesEngineering(filename, database_folder):
    df_data = read_data(os.path.join(database_folder, filename))
 
    # Data Type Conversion
    df_data = cat_adv_processing(df_data, filename)
    df_data = num_adv_processing(df_data, filename)
    num_df_data, nor_group_a_df_data, std_group_a_df_data, nor_group_b_df_data, std_group_b_df_data, nor_group_c_df_data_1_pca_data, nor_group_c_df_data_2_pca_data, std_group_c_df_data_1_pca_data, std_group_c_df_data_2_pca_data = adv_features_transformation(df_data, filename, None)
    label_data_type = ['Survived']

    # Original Data
    df_data.to_csv(os.path.join(database_folder, filename + "_features_engineering.csv"), index=False)

    # Data Concat 
    num_group = pd.concat([df_data.loc[:,'PassengerId'], num_df_data, df_data.loc[:,label_data_type]],axis = 1)

    # Data Concat
    nor_group = pd.concat([df_data.loc[:,'PassengerId'], nor_group_a_df_data, nor_group_b_df_data, nor_group_c_df_data_1_pca_data, df_data.loc[:,label_data_type]],axis = 1)
    std_group = pd.concat([df_data.loc[:,'PassengerId'], std_group_a_df_data, std_group_b_df_data, std_group_c_df_data_1_pca_data, df_data.loc[:,label_data_type]],axis = 1)

    # Data Storing
    num_group.to_csv(os.path.join(database_folder, filename + "_adv_num_group.csv"), index=False)
    nor_group.to_csv(os.path.join(database_folder, filename + "_adv_nor_group.csv"), index=False)
    std_group.to_csv(os.path.join(database_folder, filename + "_adv_std_group.csv"), index=False)

def AdvFeaturesEngineeringFit(filename, database_folder, model_file_path):
    df_data = read_data(os.path.join(database_folder, filename))
    
    # Data Type Conversion
    df_data = cat_adv_processing(df_data, filename)
    df_data = num_adv_processing(df_data, filename)
    num_df_data, nor_group_a_df_data, std_group_a_df_data, nor_group_b_df_data, std_group_b_df_data, nor_group_c_df_data_1_pca_data, nor_group_c_df_data_2_pca_data, std_group_c_df_data_1_pca_data, std_group_c_df_data_2_pca_data = adv_features_transformation(df_data, filename, model_file_path)

    # Original Data
    df_data.to_csv(os.path.join(database_folder, filename + "_features_engineering.csv"), index=False)

    # Data Concat 
    num_group = pd.concat([df_data.loc[:,'PassengerId'], num_df_data],axis = 1)

    # Data Concat
    nor_group = pd.concat([df_data.loc[:,'PassengerId'], nor_group_a_df_data, nor_group_b_df_data, nor_group_c_df_data_1_pca_data],axis = 1)
    std_group = pd.concat([df_data.loc[:,'PassengerId'], std_group_a_df_data, std_group_b_df_data, std_group_c_df_data_1_pca_data],axis = 1)

    # Data Storing
    num_group.to_csv(os.path.join(database_folder, filename + "_adv_num_group.csv"), index=False)
    nor_group.to_csv(os.path.join(database_folder, filename + "_adv_nor_group.csv"), index=False)
    std_group.to_csv(os.path.join(database_folder, filename + "_adv_std_group.csv"), index=False)
    
"""
Statistics Analysis From Models

Note
Name: Object Data Type -> Categorical Data Type
Ticket: Object Data Type -> Numerical Data Type & Categorical Data Type
Cabin: Object Data Type -> Categorical Data Type
"""

def EDAStatisticsFromModels(model_folder):
    numerical_data_type = ['Age', 'SibSp', 'Parch', 'Ticket', 'Fare']
    bi_cat_data_type = ['Sex']
    ord_cat_data_type = ['Pclass']
    nom_cat_data_type = ['Name', 'Embarked'] # 0: , 1: 
    if 'std' in model_folder:
        tag_name = 'std'
    if 'nor' in model_folder:
        tag_name = 'nor'	
    for data_type in numerical_data_type:    
        missing_data_handle = {}
        missing_data_handle['type'] = 'dropna'
        MMT.df_data_features_err_multi_model_type_plot(model_folder, data_type, 'abs_err', outputFolder, tag_name, plot_type = 'Box', ylabel_list = ["No of Passengers"], fig_size = (25, 10), missing_data_handle = missing_data_handle, FeaturesEngineeringObj = cat_mapping_visualization)
    
    for data_type in nom_cat_data_type:
        missing_data_handle = {}
        missing_data_handle['type'] = 'dropna'
        MMT.df_data_features_err_multi_model_type_plot(model_folder, data_type, 'abs_err', outputFolder, tag_name, plot_type = 'Count', ylabel_list = ["No of Passengers"], fig_size = (25, 10), missing_data_handle = missing_data_handle, FeaturesEngineeringObj = cat_mapping_visualization)
    
    for data_type in bi_cat_data_type:
        missing_data_handle = {}
        missing_data_handle['type'] = 'dropna'
        MMT.df_data_features_err_multi_model_type_plot(model_folder, data_type, 'abs_err', outputFolder, tag_name, plot_type = 'Count', ylabel_list = ["No of Passengers"], fig_size = (25, 10), missing_data_handle = missing_data_handle, FeaturesEngineeringObj = cat_mapping_visualization)

    for data_type in ord_cat_data_type:
        missing_data_handle = {}
        missing_data_handle['type'] = 'dropna'
        MMT.df_data_features_err_multi_model_type_plot(model_folder, data_type, 'abs_err', outputFolder, tag_name, plot_type = 'Count', ylabel_list = ["No of Passengers"], fig_size = (25, 10), missing_data_handle = missing_data_handle, FeaturesEngineeringObj = cat_mapping_visualization)

"""
def data2numpy(dataset):
    # Mapping Name
    dataset['NameTitle'] = dataset['Name'].apply(get_title)
    dataset['NameTitle'] = dataset['NameTitle'].replace(
        ['Lady', 'Countess', 'Capt', 'Col', 'Don', 'Dr', 'Major', 'Rev', 'Sir', 'Jonkheer', 'Dona'], 'Rare')

    dataset['NameTitle'] = dataset['NameTitle'].replace('Mlle', 'Miss')
    dataset['NameTitle'] = dataset['NameTitle'].replace('Ms', 'Miss')
    dataset['NameTitle'] = dataset['NameTitle'].replace('Mme', 'Mrs')
    title_mapping = {"Mr": 1, "Miss": 2, "Mrs": 3, "Master": 4, "Rare": 5}
    dataset['NameTitle'] = dataset['NameTitle'].map(title_mapping)
    dataset['NameTitle'] = dataset['NameTitle'].fillna(0)

    # Mapping Sex
    dataset['Sex'] = dataset['Sex'].map({'female': 0, 'male': 1}).astype(int)

    # Mapping Embarked
    freq_port = dataset.Embarked.dropna().mode()[0]
    dataset['Embarked'] = dataset['Embarked'].fillna(freq_port)
    dataset['Embarked'] = dataset['Embarked'].map({'S': 0, 'C': 1, 'Q': 2}).astype(int)

    # Age Process
    age_avg = dataset['Age'].mean()
    age_std = dataset['Age'].std()
    age_null_count = dataset['Age'].isnull().sum()
    age_null_random_list = np.random.randint(age_avg - age_std, age_avg + age_std, size=age_null_count)
    dataset['Age'][np.isnan(dataset['Age'])] = age_null_random_list
    dataset['Age'] = dataset['Age'].astype(int)

    # Mapping Fare
    fare_avg = dataset['Fare'].mean()
    fare_std = dataset['Fare'].std()
    fare_null_count = dataset['Fare'].isnull().sum()
    fare_null_random_list = np.random.randint(fare_avg - age_std, fare_avg + age_std, size=fare_null_count)
    dataset['Fare'][np.isnan(dataset['Fare'])] = fare_null_random_list
    
    dataset.loc[dataset['Fare'] <= 7.91, 'Fare'] = 0
    dataset.loc[(dataset['Fare'] > 7.91) & (dataset['Fare'] <= 14.454), 'Fare'] = 1
    dataset.loc[(dataset['Fare'] > 14.454) & (dataset['Fare'] <= 31), 'Fare'] = 2
    dataset.loc[dataset['Fare'] > 31, 'Fare'] = 3
    dataset['Fare'] = dataset['Fare'].astype(int)

    # Mapping Age
    dataset.loc[dataset['Age'] <= 16, 'Age'] = 0
    dataset.loc[(dataset['Age'] > 16) & (dataset['Age'] <= 32), 'Age'] = 1
    dataset.loc[(dataset['Age'] > 32) & (dataset['Age'] <= 48), 'Age'] = 2
    dataset.loc[(dataset['Age'] > 48) & (dataset['Age'] <= 64), 'Age'] = 3
    dataset.loc[dataset['Age'] > 64, 'Age'] = 4

    # Merge the Features
    dataset['FamilySize'] = dataset['SibSp'] + dataset['Parch'] + 1
    dataset['IsAlone'] = 0
    dataset.loc[dataset['FamilySize'] == 1, 'IsAlone'] = 1

    # Dropping Feature List
    drop_list = ['PassengerId', 'Name', 'Ticket', 'Cabin', 'Ticket', 'SibSp', 'Parch']
    dataset = dataset.drop(drop_list, axis=1)
    #print(dataset.shape)

    return dataset

def MultiModelsDataInverse(model_folder, inv_type, inv_model_path):
    err_file_list = glob.glob(os.path.join(model_folder,'*','*_err.csv'))
    for file_path in err_file_list:
        output_path = os.path.dirname(file_path)
        filename = file_path.split('\\')[-1].split('_')[0]
		
        df_data = pd.read_csv(file_path)
        InverseFeaturesEngineering(df_data, inv_type, inv_model_path, output_path, filename)

def InverseFeaturesEngineering(df_data, inv_type, inv_model_path, output_path, filename):
    numerical_data_type = ['Age', 'SibSp', 'Parch', 'Ticket', 'Fare'] #'PassengerId'
    nom_cat_data_type = ['Name', 'Embarked']
    inv_nom_cat_data_type = ['x0_Master','x0_Miss','x0_Mr','x0_Mrs','x0_Rare','x1_C','x1_Q','x1_S','x1_unknown']
    ord_cat_data_type = ['Pclass']
    bi_cat_data_type = ['Sex']
    label_data_type = ['Survived']
    drop_out_data_type = ['Cabin']  # Categorical Data

    if ('std' == inv_type):
	    # Numerical Data Type: Inverse Standardization, Inverse Normalization
        with open(inv_model_path + "_std.sav", 'rb') as file:
             stdDataObj = pickle.load(file)
        inv_std_num_data = stdDataObj.inverse_transform(df_data.loc[:, numerical_data_type])
        inv_std_num_data = pd.DataFrame(inv_std_num_data, columns=numerical_data_type)

    if ('nor' == inv_type):
        inv_nor_num_data = df_data.loc[:, numerical_data_type]
        #with open(inv_model_path + "_nor.sav", 'rb') as file:
        #     norDataObj = pickle.load(file)
        #     #print(norDataObj.get_params())
        #inv_nor_num_data = norDataObj.inverse_transform(df_data.loc[:, numerical_data_type])
        #inv_nor_num_data = pd.DataFrame(inv_nor_num_data, columns=numerical_data_type)
	
    # Ordinal Catagorical Data Type:
    with open(inv_model_path + "_ord.sav", 'rb') as file:
        ordObj = pickle.load(file)
    inv_ord_cat_data = ordObj.inverse_transform(df_data.loc[:, ord_cat_data_type])
    inv_ord_cat_data = pd.DataFrame(inv_ord_cat_data, columns=ord_cat_data_type)
    
    # Nominal Categorical Data Type: One Hot Encoder + Imputation
    with open(inv_model_path + "_ohe.sav", 'rb') as file:
        oheObj = pickle.load(file)
    df_data['x1_unknown'] = df_data['x1_S']*0
    inv_ohe_cat_data = oheObj.inverse_transform(df_data.loc[:, inv_nom_cat_data_type])
    inv_ohe_cat_data = pd.DataFrame(inv_ohe_cat_data)

    # Data Storing
    if ('std' == inv_type):
        std_ohe = pd.concat(
        [inv_std_num_data, inv_ord_cat_data, inv_ohe_cat_data, df_data.loc[:, bi_cat_data_type], df_data.loc[:,'Pred'], df_data.loc[:,'abs_err']], axis=1)
        std_ohe.to_csv(os.path.join(output_path, filename + "_std_ord_ohe_bi_inv.csv"), index=False)

    if ('nor' == inv_type):   
        nor_ohe = pd.concat(
        [inv_nor_num_data, inv_ord_cat_data, inv_ohe_cat_data, df_data.loc[:, bi_cat_data_type], df_data.loc[:,'Pred'], df_data.loc[:,'abs_err']], axis=1)
        nor_ohe.to_csv(os.path.join(output_path, filename + "_nor_ord_ohe_bi_inv.csv"), index=False) 
"""
