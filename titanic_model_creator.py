'''
@Author: CY Peng
@Date: 2019-07-25
@LastEditTime: 2019-12-02

DataVer  - Author - Note
2019/07/25    CY    First Release(20190725-R 1.0.0)
2019/11/27    CY    ML Lib New Function Included
2019/12/02    CY    Add the Models Configeration
2019/12/23    CY    add the advanced features engineering and training stage
'''

import pandas as pd
import os
import numpy as np

import SamplingLib as SM
import bclfModelCreatorHelper as MCH
import ModelSelector as MS

import titanic_preprocessing as titanic_db
import pickle
from configparser import ConfigParser

from mlxtend.feature_selection import ColumnSelector
from mlxtend.classifier import EnsembleVoteClassifier
from sklearn.pipeline import make_pipeline

def read_data(filename):
    data = pd.read_csv(filename+".csv")
    return data

def nested_training_stage(filename, database_folder, train_file_tail_tag = None,
                          features_engineering_model_path = os.path.join(".","EDA_output","train"), 
                          model_config_filename='models_config_test'):
    # Parameters Setting
    conf_matrix_params = {}
    conf_matrix_params['confusion_matrix'] = {}
    conf_matrix_params['confusion_matrix']['labels'] = [1, 0]
    conf_matrix_params['clf_report'] = {}
    conf_matrix_params['clf_report']['digits'] = 2

    # Model Selection
    classifiers, grid_param =  MS.model_selector(os.path.join(os.path.dirname(os.path.realpath(__file__)), model_config_filename))

    # Data Read
    rawData = read_data(os.path.join(database_folder, filename))
    raw_y = rawData.loc[:, 'Survived'].to_frame()
    raw_x = rawData.drop(columns=['Survived'], axis=1) #'PassengerId', 

    # Training Stage
    ModelHelper = MCH.MultiModelMLHelper(classifiers, grid_param)
    outer_fold_obj, inner_fold_obj = SM.two_stages_sampling_stratified_shuffle_split_obj()
    ModelHelper.data_preparation(raw_x, raw_y, train_file_tail_tag, features_engineering_model_path, titanic_db.FeaturesEngineeringFit,train_drop_column = ['PassengerId'])
    ModelHelper.nested_train(inner_fold_obj, outer_fold_obj, conf_matrix_params)

def adv_nested_training_stage(filename, database_folder, train_file_tail_tag = None,
                              features_engineering_model_path = os.path.join(".","EDA_output","train"), 
                              model_config_filename='models_config_test'):
	# Parameters Setting
    conf_matrix_params = {}
    conf_matrix_params['confusion_matrix'] = {}
    conf_matrix_params['confusion_matrix']['labels'] = [1, 0]
    conf_matrix_params['clf_report'] = {}
    conf_matrix_params['clf_report']['digits'] = 2

    # Model Selection
    classifiers, grid_param =  MS.model_selector(os.path.join(os.path.dirname(os.path.realpath(__file__)), model_config_filename))

    # Data Read
    rawData = read_data(os.path.join(database_folder, filename))
    raw_y = rawData.loc[:, 'Survived'].to_frame()
    raw_x = rawData.drop(columns=['Survived'], axis=1) #'PassengerId', 

    # Training Stage
    ModelHelper = MCH.MultiModelMLHelper(classifiers, grid_param)
    outer_fold_obj, inner_fold_obj = SM.two_stages_sampling_stratified_shuffle_split_obj()
    ModelHelper.data_preparation(raw_x, raw_y, train_file_tail_tag, features_engineering_model_path, titanic_db.AdvFeaturesEngineeringFit,train_drop_column = ['PassengerId'])
    ModelHelper.nested_train(inner_fold_obj, outer_fold_obj, conf_matrix_params)

def prediction_stage(filename, database_folder, model_folder, modelname):
    data = read_data(os.path.join(database_folder, filename))
    #data = data.drop(columns=['PassengerId', 'Survived'], axis=1)
    loaded_model = pickle.load(open(os.path.join(model_folder, modelname+".sav"), 'rb'))
    submission = pd.DataFrame({
        "PassengerId": data['PassengerId'],
		"Survived": loaded_model.predict(data.drop(columns=['PassengerId'], axis=1))
        #"Survived": loaded_model.predict(np.array(data.drop(columns=['PassengerId'], axis=1))[0::, 0::])
    })
    submission.to_csv('submission.csv', index=False)

def multi_models_prediction_stage(database_folder, train_filename, test_filename,
                                  model_folder_list, modelname_list, column_selector_list,
                                  **kwargs):
    
    train_data = read_data(os.path.join(database_folder, train_filename))
    test_data = read_data(os.path.join(database_folder, test_filename))
    y_train = train_data.loc[:, 'Survived'].to_frame()
    X_train = train_data.drop(columns=['Survived'], axis=1) #'PassengerId',
    
    # Model Load
    pipelinelist = []
    #print(kwargs)
    for idx, model_folder in enumerate(model_folder_list):
        modelname = modelname_list[idx]
        loaded_model = pickle.load(open(os.path.join(model_folder, modelname+".sav"), 'rb'))
        pipeline = make_pipeline(ColumnSelector(cols=column_selector_list[idx]), loaded_model)
        pipelinelist.append(pipeline)
    
    # Escemble Model
    eclf = EnsembleVoteClassifier(clfs=pipelinelist, **kwargs)#weights=[1,0.4,0.6], refit=False)
    
    # Model Prediction
    submission = pd.DataFrame({
        "PassengerId": test_data['PassengerId'],
    	"Survived": eclf.fit(X_train, y_train).predict(test_data)
        #"Survived": loaded_model.predict(np.array(data.drop(columns=['PassengerId'], axis=1))[0::, 0::])
    })
    submission.to_csv('submission.csv', index=False)